import 'dart:ui';

const colorOrg = Color(0xFFEF5524);
const colorQmk = Color(0xFF071CFD);
const colorBtn = Color(0xCECBCBFF);
const colorTextBnt = Color(0x2020215B);
const colorDialog = Color(0xFFEEDAD1);
const colorContainerImage = Color(0x9ED0CDCD);