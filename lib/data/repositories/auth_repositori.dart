import 'package:firebase_auth/firebase_auth.dart';

class AuthRepositori{
 final _firebaseAth =  FirebaseAuth.instance;
 Future<void> signUp({required String email, required String pass}) async{
   try{
          FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: pass);
   }on FirebaseAuthException catch(e){
     if(e.code == 'weak-password'){
       throw Exception('this is pass is too');
     }else if(e.code == 'email-already-in-use'){
       throw Exception('the account already exits for that email');
     }

   }catch(e){
     throw Exception(e.toString());
   }
 }

}