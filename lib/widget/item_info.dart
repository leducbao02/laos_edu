import 'package:flutter/material.dart';
import 'package:laos_edu/ui/color.dart';
import 'package:laos_edu/widget/button.dart';

class ItemInfor extends StatefulWidget {
  const ItemInfor({Key? key, required this.title, required this.content, required this.icon,}) : super(key: key);
  final String title;
  final String content;
  final IconData icon;
  @override
  State<ItemInfor> createState() => _ItemInforState();
}

class _ItemInforState extends State<ItemInfor> {
  @override
  Widget build(BuildContext context) {
      return Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: colorDialog
                  ),
                  child: Icon(widget.icon ,color: colorOrg,),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.title,style: const TextStyle(
                          fontSize: 12,
                          color: Colors.grey
                        ),),
                        const SizedBox(height: 5,),
                        Text(widget.content,style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                        ),),
                        const SizedBox(height: 10,),
                        const SizedBox(width: double.maxFinite,child: Divider(height: 10, thickness: 1,),),

                      ],
                    ),
                  ),
                ),

              ],
            ),
        );
  }
}
