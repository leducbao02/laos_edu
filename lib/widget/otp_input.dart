import 'package:flutter/material.dart';

import '../ui/color.dart';

class OtpInput extends StatefulWidget {
  final TextEditingController controller;
  final String? text;
  final bool autoFocus;
  final bool? checkColorBoder;
  const OtpInput(this.controller, this.autoFocus, {Key? key, this.text, this.checkColorBoder}) : super(key: key);

  @override
  State<OtpInput> createState() => _OtpInputState();
}

class _OtpInputState extends State<OtpInput> {
  @override
  Widget build(BuildContext context) {
    checkColorBoder(){
      if(widget.checkColorBoder == true){
        return  const OutlineInputBorder(
          borderSide: BorderSide(color: colorOrg),
        );
      }else {
        return const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        );
      }
    }
    return SizedBox(
      height: 40,
      width: 40,
      child: TextField(
        autofocus: widget.autoFocus,
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        controller: widget.controller,
        maxLength: 1,

        cursorColor: Theme.of(context).primaryColor,
        decoration:  InputDecoration(
            focusedBorder:  checkColorBoder(),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(7)
            ),
            counterText: '',
            hintStyle: const TextStyle(color: Colors.black, fontSize: 20.0)),
        onChanged: (value) {
          if (value.length == 1) {
            FocusScope.of(context).nextFocus();
          }
        },
      ),
    );
  }
}