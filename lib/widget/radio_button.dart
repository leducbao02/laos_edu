import 'package:flutter/material.dart';
import 'package:laos_edu/ui/color.dart';

enum SingingCharacter { lafayette, jefferson }
class CustomRadio extends StatefulWidget {
  const CustomRadio({super.key, required this.text1, required this.text2, required this.titele,  this.rate});
  final String titele;
  final String text1;
  final String text2;
  final bool? rate;

  @override
  State<CustomRadio> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<CustomRadio> {
  SingingCharacter? _character = SingingCharacter.lafayette;
  showText(){
   if(widget.rate != null && widget.rate == true){
     return const Text(
       '*',
       style: TextStyle(
         color: Colors.red,
         fontWeight: FontWeight.bold
       ),
     );
   }
   return Container();
  }

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: <Widget>[
        Row(
          children: [
            Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
                child:  Text(widget.titele,style: const TextStyle(
                    fontSize: 14,fontWeight: FontWeight.bold
                ),)),
            const SizedBox(width: 4,),
            showText()
          ],
        ),
        ListTile(
          title:  Text(widget.text1),
          leading: Radio<SingingCharacter>(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            value: SingingCharacter.lafayette,
            groupValue: _character,
            activeColor: colorOrg,
            onChanged: (SingingCharacter? value) {
              setState(() {
                _character = value;
              });
            },
          ),
        ),
        ListTile(
          title:  Text(widget.text2),
          leading: Container(
            margin:  const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Radio<SingingCharacter>(

              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              value: SingingCharacter.jefferson,
              groupValue: _character,
              activeColor: colorOrg,
              onChanged: (SingingCharacter? value) {
                setState(() {
                  _character = value;
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}