import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:laos_edu/dialog/dialog_opt_success.dart';
import '../ui/color.dart';
import '../widget/otp_input.dart';

class DialogOtp extends StatefulWidget {
  const DialogOtp({Key? key}) : super(key: key);

  @override
  State<DialogOtp> createState() => _DialogForgetPassState();
}

class _DialogForgetPassState extends State<DialogOtp> {
  late TextEditingController textEditingController;
  late TextEditingController textEditingController1;
  late TextEditingController textEditingController2;
  late TextEditingController textEditingController3;
  late TextEditingController textEditingController4;
  late TextEditingController textEditingController5;

  @override
  void initState() {
    textEditingController = TextEditingController();
    textEditingController1 = TextEditingController();
    textEditingController2 = TextEditingController();
    textEditingController3 = TextEditingController();
    textEditingController4 = TextEditingController();
    textEditingController5 = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      title: const Center(
          child: Text(
            'LẤY LẠI MẬT KHẨU',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
          )),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      content:
      // height: 500,
      // width: double.infinity,
      SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 330,
        child: Column(
          children: [
            const SizedBox(height: 10,),
            const Divider(),
            const SizedBox(height: 10,),
            const Center(child: Text('Yêu cầu nhập mã xác minh hệ thống đã gửi',style: TextStyle(
              fontSize: 13,fontWeight: FontWeight.bold
            ),)),
            const SizedBox(height: 4,),
            const Center(child: Text('đến email mà bạn đã đăng ký:',style: TextStyle(
                fontSize: 13,fontWeight: FontWeight.bold

            ),)),
            const SizedBox(height: 30,),
            const Text('Mã xác minh',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            const SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OtpInput(textEditingController,true,checkColorBoder: true),
                const SizedBox(width: 5,),
                OtpInput(textEditingController1,true,checkColorBoder: true),
                const SizedBox(width: 5,),
                OtpInput(textEditingController2,true,checkColorBoder: true),
                const SizedBox(width: 5,),
                OtpInput(textEditingController3,true,checkColorBoder: true),
                const SizedBox(width: 5,),
                OtpInput(textEditingController4,true,checkColorBoder: true),
                const SizedBox(width: 5,),
                OtpInput(textEditingController5,true,checkColorBoder: true),

              ],
            ),
            const SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Vui lòng chờ 120 giây để ',style: TextStyle(fontSize: 11),),
                Text('Gửi lại mã ', style: TextStyle(
                  color: Colors.blue,
                  fontSize: 11
                ),),
              ],
            ),
            const SizedBox(height: 20,),
            const Divider(),
            const SizedBox(height: 15,),
            SizedBox(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                        width: 110,
                        height: 34,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: colorBtn
                        ),
                        child: const Center(child: Text('Hủy bỏ',style: TextStyle(color: Colors.black54,fontSize: 13),))),
                  ),
                  const SizedBox(width: 15,),
                  Container(
                      width: 110,
                      height: 34,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: colorOrg
                      ),
                      child: InkWell(
                          onTap: (){
                            if(textEditingController.text.isEmpty || textEditingController1.text.isEmpty || textEditingController2.text.isEmpty || textEditingController3.text.isEmpty || textEditingController4.text.isEmpty || textEditingController5.text.isEmpty ){
                              Flushbar(
                                margin: const EdgeInsets.all(10),
                                icon: Image.asset(
                                  'images/cancel.png',
                                  width: 40,
                                  height: 48,
                                ),
                                mainButton: Image.asset(
                                  'images/close.png',
                                  width: 30,
                                  height: 30,
                                ),
                                flushbarPosition: FlushbarPosition.TOP,
                                borderRadius: BorderRadius.circular(10),
                                borderColor: colorOrg,
                                titleText: Container(
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: const Text('Thất bại',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold))),
                                messageText: Container(
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: const Text(
                                      'Bạn phải nhập đủ OTP',
                                      style: TextStyle(color: Colors.grey, fontSize: 12),
                                    )),
                                duration: const Duration(seconds: 3),
                                backgroundColor: colorDialog,
                              ).show(context);
                            }else{
                              Navigator.pop(context);
                              showDialog(
                                context: context, builder: (context) {
                                return  DialogOtpSuccess(textEditingController: textEditingController,textEditingController1: textEditingController1,textEditingController2: textEditingController2,textEditingController3: textEditingController3,textEditingController4: textEditingController4,textEditingController5: textEditingController5,);
                              },);
                            }

                          },
                          child: const Center(child: Text('Xác nhận',style: TextStyle(color: Colors.white,fontSize: 13),))))
                ],
              ),
            )

          ],
        ),
      ),
    );
  }
}
