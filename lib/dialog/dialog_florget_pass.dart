import 'package:flutter/material.dart';
import 'package:laos_edu/dialog/dialog_otp.dart';
import 'package:laos_edu/widget/radio_button.dart';
import 'package:laos_edu/widget/text_input.dart';

import '../ui/color.dart';

class DialogForgetPass extends StatefulWidget {
  const DialogForgetPass({Key? key}) : super(key: key);

  @override
  State<DialogForgetPass> createState() => _DialogForgetPassState();
}

class _DialogForgetPassState extends State<DialogForgetPass> {
  late TextEditingController textEditingController;

  @override
  void initState() {
    textEditingController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: AlertDialog(
        contentPadding: EdgeInsets.zero,
        title: const Center(
            child: Text(
          'LẤY LẠI MẬT KHẨU',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
        )),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        content:
            // height: 500,
            // width: double.infinity,
            SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 500,
          child: Column(
            children: [
              const SizedBox(
                height: 15,
              ),
              const Divider(
                color: Colors.grey,
              ),
              const SizedBox(
                height: 10,
              ),
              const CustomRadio(
                  text1: 'Laosedu',
                  text2: 'Ulearn',
                  titele: 'Lấy lại mật khẩu cho tài khoản'),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
                  child: const Text(
                    'Nhập tên đăng nhập muốn lấy lại mật khẩu:',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  )),
              const SizedBox(
                height: 15,
              ),
              InputText(
                title: 'Tên đăng nhập',
                textEditingController: textEditingController,
                hintText: 'Nhập tên đăng nhập',
                required: true,
              ),
              const SizedBox(
                height: 10,
              ),
              const CustomRadio(
                  text1: 'Số điện thoại đăng ký',
                  text2: 'Email đã đăng ký',
                  titele: 'Gửi mã OTP xác nhận qua',
                  rate: true),
              const Divider(
                height: 5,
                color: Colors.black,
              ),
              const SizedBox(height: 10,),
              SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 110,
                        height: 34,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: colorBtn
                        ),
                        child: const Center(child: Text('Hủy bỏ',style: TextStyle(color: Colors.black54,fontSize: 13),))),
                    ),
                    const SizedBox(width: 15,),
                    InkWell(
                      onTap: (){
                        Navigator.pop(context);
                        showDialog(
                          context: context, builder: (context) {
                          return const DialogOtp();
                        },);
                      },
                      child: Container(
                          width: 110,
                          height: 34,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: colorOrg
                          ),
                          child: const Center(child: Text('Gửi mã',style: TextStyle(color: Colors.white,fontSize: 13),))),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
