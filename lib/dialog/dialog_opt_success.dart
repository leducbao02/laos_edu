import 'package:flutter/material.dart';
import '../ui/color.dart';
import '../widget/otp_input.dart';

class DialogOtpSuccess extends StatefulWidget {

  const DialogOtpSuccess({Key? key, required this.textEditingController, required this.textEditingController1, required this.textEditingController2, required this.textEditingController3, required this.textEditingController4, required this.textEditingController5}) : super(key: key);
  final TextEditingController textEditingController;
  final TextEditingController textEditingController1;
  final TextEditingController textEditingController2;
  final TextEditingController textEditingController3;
  final TextEditingController textEditingController4;
  final TextEditingController textEditingController5;


  @override
  State<DialogOtpSuccess> createState() => _DialogForgetPassState();
}

class _DialogForgetPassState extends State<DialogOtpSuccess> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      title: const Center(
          child: Text(
            'LẤY LẠI MẬT KHẨU',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
          )),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      content:
      // height: 500,
      // width: double.infinity,
      SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 350,
        child: Column(
          children: [
            const SizedBox(height: 10,),
            const Divider(),
            const SizedBox(height: 10,),
            const Center(child: Text('Yêu cầu nhập mã xác minh hệ thống đã gửi',style: TextStyle(
                fontSize: 13,fontWeight: FontWeight.bold
            ),)),
            const SizedBox(height: 4,),
            const Center(child: Text('đến email mà bạn đã đăng ký:',style: TextStyle(
                fontSize: 13,fontWeight: FontWeight.bold

            ),)),
            const SizedBox(height: 30,),
            const Text('Mã xác minh',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            const SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OtpInput(widget.textEditingController,true),
                const SizedBox(width: 5,),
                OtpInput(widget.textEditingController1,true),
                const SizedBox(width: 5,),
                OtpInput(widget.textEditingController2,true),
                const SizedBox(width: 5,),
                OtpInput(widget.textEditingController3,true),
                const SizedBox(width: 5,),
                OtpInput(widget.textEditingController4,true),
                const SizedBox(width: 5,),
                OtpInput(widget.textEditingController5,true),

              ],
            ),
            const SizedBox(height: 15,),
            const Center(
              child: Text('Xác thực thành công!',style: TextStyle(color: Colors.green, fontSize: 13),),
            ),
            SizedBox(height: 5,),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Vui lòng kiểm tra email bạn đã đăng ký để lấy lại',style: TextStyle(fontSize: 13),),
                Text('mật khẩu', style: TextStyle(
                    fontSize: 13
                ),),
              ],
            ),
            const SizedBox(height: 20,),
            const Divider(),
            const SizedBox(height: 15,),
            Center(
              child: InkWell(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Container(
                    width: 90,
                    height: 34,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: colorBtn
                    ),
                    child: const Center(child: Text('Đóng',style: TextStyle(color: Colors.black54,fontSize: 13),))),
              ),
            )

          ],
        ),
      ),
    );
  }
}
