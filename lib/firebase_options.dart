// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyAV-NHcXDTUSBdIdw-lBsjeiLnpml4mScg',
    appId: '1:1088826266451:web:89732ded9231cd699947a6',
    messagingSenderId: '1088826266451',
    projectId: 'laoseedu-4affa',
    authDomain: 'laoseedu-4affa.firebaseapp.com',
    storageBucket: 'laoseedu-4affa.appspot.com',
    measurementId: 'G-8DXK462CYV',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyB0KIPmuZBbG9z7XJ6kRXmYkrbRlRuuInE',
    appId: '1:1088826266451:android:f71411d967fda0e39947a6',
    messagingSenderId: '1088826266451',
    projectId: 'laoseedu-4affa',
    storageBucket: 'laoseedu-4affa.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyAMFalYVpr6GWv8uXpkIyhteYLeqCht9Vk',
    appId: '1:1088826266451:ios:a12fa624352373a39947a6',
    messagingSenderId: '1088826266451',
    projectId: 'laoseedu-4affa',
    storageBucket: 'laoseedu-4affa.appspot.com',
    androidClientId: '1088826266451-mrag22rg0i7sr16oo457ut9ln4ps7e8s.apps.googleusercontent.com',
    iosClientId: '1088826266451-m8ac6gtq0j3r98o3esfgdrmk55gf2sfp.apps.googleusercontent.com',
    iosBundleId: 'com.example.laosEdu',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyAMFalYVpr6GWv8uXpkIyhteYLeqCht9Vk',
    appId: '1:1088826266451:ios:a12fa624352373a39947a6',
    messagingSenderId: '1088826266451',
    projectId: 'laoseedu-4affa',
    storageBucket: 'laoseedu-4affa.appspot.com',
    androidClientId: '1088826266451-mrag22rg0i7sr16oo457ut9ln4ps7e8s.apps.googleusercontent.com',
    iosClientId: '1088826266451-m8ac6gtq0j3r98o3esfgdrmk55gf2sfp.apps.googleusercontent.com',
    iosBundleId: 'com.example.laosEdu',
  );
}
