import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:laos_edu/Screens/home_screen.dart';
import 'package:laos_edu/Screens/login_screen.dart';
import 'package:laos_edu/dialog/dialog_otp.dart';
import 'package:laos_edu/widget/item_info.dart';
import 'package:laos_edu/widget/radio_button.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
 await Firebase.initializeApp();

  FirebaseAuth.instance
      .authStateChanges()
      .listen((User? user) {
    if (user == null) {
      runApp( const MaterialApp(
          debugShowCheckedModeBanner: false,
          home: LoginScreen()));
    } else {
      runApp( const MaterialApp(
          debugShowCheckedModeBanner: false,
          home: HomeScreen()));
    }
  });

}

