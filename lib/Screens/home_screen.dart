import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:laos_edu/Screens/login_screen.dart';
import 'package:laos_edu/models/infor_model.dart';
import 'package:laos_edu/ui/color.dart';
import 'package:laos_edu/widget/item_info.dart';

import '../widget/button.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  InforModel list = InforModel(
      'images/aaa.jpg',
      'Nguyễn Văn A',
      'NH2-MSV18011',
      'baoleduc02@gmail.com',
      '0343745088',
      'Nam',
      '03-07-2002',
      'NH2- THPT Nguyễn Huệ',
      '10A13');

  Future<void> logout() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    googleSignIn.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(
              height: 25,
            ),
            const Center(
              child: Text(
                'Hồ sơ của tôi',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: colorContainerImage),
                child: Row(
                  children: [
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        image: DecorationImage(
                          image: AssetImage(list.imageUser),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      list.name,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 14,
            ),
            ItemInfor(
              icon: Icons.supervised_user_circle_outlined,
              title: 'Account đăng nhập',
              content: list.nameAccount,
            ),
            const SizedBox(
              height: 10,
            ),
            ItemInfor(
              icon: Icons.email_outlined,
              title: 'Email',
              content: list.email,
            ),
            const SizedBox(
              height: 10,
            ),
            ItemInfor(
              icon: Icons.phone,
              title: 'Số điện thoại',
              content: list.phoneNumber,
            ),
            const SizedBox(
              height: 10,
            ),
            ItemInfor(
              icon: Icons.date_range_rounded,
              title: 'Ngày sinh',
              content: list.date,
            ),
            const SizedBox(
              height: 10,
            ),
            ItemInfor(
              icon: Icons.supervised_user_circle_outlined,
              title: 'Giới tính',
              content: list.sex,
            ),
            const SizedBox(
              height: 10,
            ),
            ItemInfor(
              icon: Icons.school,
              title: 'Trường học',
              content: list.school,
            ),
            const SizedBox(
              height: 10,
            ),
            ItemInfor(
              icon: Icons.class_,
              title: 'Lớp học',
              content: list.myClass,
            ),
            const Divider(thickness: 1),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: const [
                CustomButton(
                  text: 'Laos Edu',
                  width1: 150,
                ),
                CustomButton(
                  text: 'U-Learn',
                  width1: 150,
                ),
              ],
            ),
            ElevatedButton(
                onPressed: () async {
                  await logout();
                  FirebaseAuth.instance.signOut();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const LoginScreen(),
                      ));
                },
                child: const Center(child: Text('Sign out'))),
          ],
        ),
      )),
    );
  }
}
