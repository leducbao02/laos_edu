import 'package:another_flushbar/flushbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:laos_edu/Screens/login_screen.dart';
import 'package:laos_edu/ui/color.dart';
import 'package:laos_edu/widget/button.dart';
import 'package:laos_edu/widget/text_input.dart';

class SignScreen extends StatefulWidget {
  const SignScreen({Key? key}) : super(key: key);

  @override
  State<SignScreen> createState() => _SignScreenState();
}

class _SignScreenState extends State<SignScreen> {
  late TextEditingController textEditingControllerName;
  late TextEditingController textEditingControllerGmail;
  late TextEditingController textEditingControllerPass;

  @override
  void initState() {
    textEditingControllerName = TextEditingController();
    textEditingControllerGmail = TextEditingController();
    textEditingControllerPass = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Sign in',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 15,
          ),
          InputText(
              title: 'Tên đăng nhập',
              textEditingController: textEditingControllerName,
              hintText: 'Nhập tên đăng nhập'),
          const SizedBox(
            height: 15,
          ),
          InputText(
              title: 'Gmail',
              textEditingController: textEditingControllerGmail,
              hintText: 'Nhập gmail của bạn'),
          const SizedBox(
            height: 15,
          ),
          InputText(
              title: 'Mật khẩu',
              textEditingController: textEditingControllerPass,
              hintText: 'Nhập mật khẩu của bạn'),
          const SizedBox(
            height: 15,
          ),
          InkWell(
              onTap: () {
                if (textEditingControllerGmail.text.isEmpty ||
                    textEditingControllerName.text.isEmpty ||
                    textEditingControllerPass.text.isEmpty) {
                  Flushbar(
                    margin: const EdgeInsets.all(10),
                    icon: Image.asset(
                      'images/cancel.png',
                      width: 40,
                      height: 48,
                    ),
                    mainButton: Image.asset(
                      'images/close.png',
                      width: 30,
                      height: 30,
                    ),
                    flushbarPosition: FlushbarPosition.TOP,
                    borderRadius: BorderRadius.circular(10),
                    borderColor: colorOrg,
                    titleText: Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: const Text('Thất bại',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.bold))),
                    messageText: Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: const Text(
                          'Không có dữ liệu',
                          style: TextStyle(color: Colors.grey, fontSize: 12),
                        )),
                    duration: const Duration(seconds: 3),
                    backgroundColor: colorDialog,
                  ).show(context);
                }
                FirebaseAuth.instance
                    .createUserWithEmailAndPassword(
                        email: textEditingControllerGmail.text,
                        password: textEditingControllerPass.text)
                    .then((value) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const LoginScreen(),
                      ));
                }).onError((error, stackTrace) {
                  print('error');
                });
              },
              child: const CustomButton(text: 'Sign up'))
        ],
      ),
    );
  }
}
